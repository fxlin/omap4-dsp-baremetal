-c
-heap 		0x100
-stack 		0x100
--disable_auto_rts
MEMORY
{
	BMEM: o = 10800000h l = 1000h /* .bss, .sysmem, .stack, .cinit */
}
SECTIONS
{
	.text 		> BMEM
	
	.data 		> BMEM
	.stack 	> BMEM
	.bss 		> BMEM
	.sysmem 	> BMEM
	.cinit 	> BMEM
	.const 	> BMEM
	.cio 		> BMEM
	.far 		> BMEM
}
