-----------------
Overview
-----------------
The experimental code for OMAP4's c64+ DSP (aka Tesla)
Update: add script and build commands for K2H's c66 DSP

-----------------
To build
-----------------
You need to install TI's c6x compiler (I use v7.4.6) and set up their paths as follows (eg in your ~/.bashrc):

```
export TI_CG6X_DIR="/home/xzl/opt/ti/cgt"
export C6X_C_DIR="/home/xzl/opt/ti/cgt/include;/home/xzl/opt/ti/cgt/lib"
export PATH=$PATH:$TI_CG6X_DIR/bin
```

./mk.panda
# or 
./mk.k2h

-----------------
Using GCC?
-----------------
Although there's GCC for c6x, I am not sure whehter it works for Tesla or not -- 
Tesla only has 4 functional units which are incompatible with most of the c6x units (8 units).


